#pragma once

#include "center_algs.h"
#include "clustering_algs.h"
#include "curve.h"

Clustering computeCenterClustering(
	Curves const& curves, int k, int l, ClusterAlg cluster_alg, CenterAlg center_alg, int max_rounds = 10);
