#include "center_clustering_algs.h"
#include "defs.h"
#include "io.h"

#include <string>

void printUsage()
{
	std::cout << "USAGE: ./main <dataset_file> <k> <l> [<header_size>]\n"
	          << "The dataset_file should contain a newline separated list of filenames of curve files.\n"
	          << "The header size (default: 1) gives the number of lines which are ignored at the beginning of each curve file.\n";
}

int main(int argc, char* argv[])
{
	int header_size = 1;

	if (argc == 5) {
		header_size = std::stoi(argv[4]);
	}
	else if (argc != 4) {
		printUsage();
		ERROR("Wrong number of arguments.");
	}

	std::string base_path = argv[1];
	int k = std::stoi(argv[2]);
	int l = std::stoi(argv[3]);

	auto curves = io::readCurves(base_path, header_size);
	auto clustering = computeCenterClustering(curves, k, l, ClusterAlg::Gonzalez, CenterAlg::FSA);

	io::exportClustering("out.clustering", base_path, clustering, curves);
	io::exportCentersGPX("centers.gpx", clustering);
}
