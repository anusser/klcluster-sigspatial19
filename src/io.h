#pragma once

#include "basic_types.h"
#include "curve.h"

namespace io
{

Curves readCurves(std::string const& curve_data_file, int header_size = 0);
void exportClustering(std::string const& filename, std::string const& base_path, Clustering const& clustering, Curves const& curves);
void exportCenters(std::string const& filename, Clustering const& clustering);
void exportCentersGPX(std::string const& filename, Clustering const& clustering);
void exportCurvesGPX(std::string const& filename, Curves const& curves);

} // end namespace io
