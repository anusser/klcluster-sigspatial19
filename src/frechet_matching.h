#pragma once

#include "curve.h"
#include "geometry_basics.h"

Points calcMatching(Curve const& curve1, Curve const& curve2);
