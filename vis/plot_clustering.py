#!/usr/bin/python3
# Plots a clustering

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys

plot_curves = True
plot_centers = True
gray_curves = True

def readCenter(f):
    center = [[],[]]
    center_str = f.readline().split()
    for x,y in zip(center_str[0::2], center_str[1::2]):
        center[0].append(x)
        center[1].append(y)
    return center

def readCluster(f, path):
    cluster = []
    filenames = f.readline().split()
    for filename in filenames:
        cluster.append(readCurve(path + "/" + filename))
    return cluster

def readCurve(filename):
    curve = [[],[]] # a curve is an x and a y list
    with open(filename, 'r') as f:
        f.readline() # read header
        for line in f:
            A = line.split()
            if A:
                curve[0].append(A[0])
                curve[1].append(A[1])
    return curve

def readClusters(clusters_file):
    centers = []
    clusters = []
    with open(clusters_file, 'r') as f:
        num_clusters = int(f.readline())
        path = f.readline()[:-1] # drop newline at the end
        for i in range(num_clusters):
            centers.append(readCenter(f))
            clusters.append(readCluster(f, path))
    return centers,clusters

def exportClustering(centers, clusters, filename):
    assert(len(centers) == len(clusters))

    colors=plt.cm.rainbow(np.linspace(0,1,len(centers)))
    clusters_to_plot = range(len(centers))
    if plot_curves:
        for i in clusters_to_plot:
            for curve in clusters[i]:
                if gray_curves:
                    plt.plot(curve[0], curve[1], color='lightgray', alpha=.3)
                else:
                    plt.plot(curve[0], curve[1], color=colors[i], alpha=.5)
    if plot_centers:
        for i in clusters_to_plot:
            plt.plot(centers[i][0], centers[i][1], '-o', markersize=6, color=colors[i], lw=3)

    plt.axis('equal')
    plt.axis('off')
    plt.savefig(filename, format='pdf', bbox_inches = 'tight', pad_inches = 0)
    plt.clf()

if len(sys.argv) == 1:
    print("USAGE: ./plot_clustering.py <clusters_files>")
    quit()

clusters_files = sys.argv[1:]

for clusters_file in clusters_files:
    centers, clusters = readClusters(clusters_file)
    pdf_filename = clusters_file.split("/")[-1] + ".pdf"
    exportClustering(centers, clusters, pdf_filename)
